const access_token = "access_token=";
const group_id = 36258159;
const topic_id = 29468829;
export const offset = 0;
const method = "execute";
const code = `code= 
var comments = API.board.getComments({"group_id":${group_id},"topic_id":${topic_id},"count": 100, "offset": ${offset}, "extended" : 1, "need_likes" : 1 }).items;
return comments;`;
export const root = `https://api.vk.com/method/${method}?${code}&v=5.62&${access_token}`;

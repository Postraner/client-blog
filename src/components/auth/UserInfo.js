/**
 * Created by root on 28.03.17.
 */

export default function setUserInfo (response, self) {
  let data = response.session.user;
  let userName = data.first_name  + " " + data.last_name;
  localStorage.setItem('user_name', userName);
  getAvatar(self);
}

function getAvatar(self) {
  VK.Api.call('users.get', {fields: "photo_50"}, function (r){
    if(r.response){
      r = r.response;
      localStorage.setItem('user_photo', r[0].photo_50);
      self.$router.push('/');
    } else alert("Не удалось получить аватар");
  });
}

import Vue from 'vue'
import Router from 'vue-router'
import Blog from '@/containers/Blog'
import Dev from '@/containers/Dev'
import Auth from '@/components/auth/Auth'

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Blog',
      component: Blog
    },
    {
      path: '/dev',
      component: Dev
    },
    {
      path: '/login',
      component: Auth
    },
  ]
})
